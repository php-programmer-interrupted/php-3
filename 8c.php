<?php
/**
 * Объедините две ранее написанные функции в одну, которая получает строку на
 * русском языке, производит транслитерацию и замену пробелов на подчеркивания
 *
 * Пример вызова скрипта из командной строки: <php 8c.php "Преобразуемая строка">
 *
 */



    /* Функция преобразования строки в массив, поддерживает UTF-8 */
    function mb_str_split($string)
    {
        return preg_split('/(?<!^)(?!$)/u', $string);
    }

    /* Функция транслитерации строки, возвращает преобразованную строку */
    function fTransliteration($strRus = '') {
        // Массив транслитерации
        $arrTransliteration = array(
            "а" => "a", "A" => "A",
            "б" => "b", "Б" => "B",
            "в" => "v", "В" => "V",
            "г" => "g", "Г" => "G",
            "д" => "d", "Д" => "D",
            "е" => "e", "Е" => "E",
            "ё" => "ye", "Ё" => "Ye",
            "ж" => "zh", "Ж" => "Zh",
            "з" => "z", "З" => "Z",
            "и" => "i", "И" => "I",
            "й" => "y", "Й" => "Y",
            "к" => "k", "К" => "K",
            "л" => "l", "Л" => "K",
            "м" => "m", "М" => "M",
            "н" => "n", "Н" => "N",
            "о" => "o", "О" => "O",
            "п" => "p", "П" => "P",
            "р" => "r", "Р" => "R",
            "с" => "s", "С" => "S",
            "т" => "t", "Т" => "T",
            "у" => "u", "У" => "U",
            "ф" => "f", "Ф" => "F",
            "х" => "kh", "Х" => "Kh",
            "ц" => "ts", "Ц" => "Ts",
            "ч" => "ch", "Ч" => "Ch",
            "ш" => "sh", "Ш" => "Sh",
            "щ" => "shch", "Щ" => "Shch",
            "ъ" => "", "Ъ" => "",
            "ы" => "y", "Ы" => "Y",
            "ь" => "", "ь" => "",
            "э" => "e", "Э" => "E",
            "ю" => "yu", "Ю" => "Yu",
            "я" => "ya", "Я" => "Ya",
        );
        $strLat = '';
        $arrRus = mb_str_split($strRus);
        foreach ($arrRus as $symRus) {
            $flPresent = FALSE;
            foreach ($arrTransliteration as $aRus => $aLat) {
                if ($symRus == $aRus) {
                    $strLat .= $aLat;
                    $flPresent = TRUE;
                }
            }
            if (!$flPresent) {
                $strLat .= $symRus;
            }
        }
        return $strLat;
    }

    /*
     * Функция замены в строке пробелов на подчеркивания
     * В качестве параметра получает строку, возвращает преобразованную строку,
     * в которой пробелы заменены символом подчеркивания
     */
    function fReplaceSpaceUnderline($strSpace = '')
    {
        $arrStr = mb_str_split($strSpace);
        $strReturn = '';
        foreach ($arrStr as $strValue) {
            if ($strValue == ' ') {
                $strValue = '_';
            }
            $strReturn .= $strValue;
        }
        return $strReturn;
    }

    /*
     * Функция транслитерации символов и замены пробелов подчёркиванием
     * На входе получает строку символов для преобразования,
     * Возвращает преобразованную строку
     */
    function fTransUnder($strInput) {
        return fReplaceSpaceUnderline(fTransliteration($strInput));;
    }


echo <<<TASK
Задание №8:
Объедините две ранее написанные функции в одну, которая получает строку на
русском языке, производит транслитерацию и замену пробелов на подчеркивания\r\n\r\n
TASK;

// Инициализируем переменные
$strRus = '';
$strTransUnderline = '';

// Получаем исходную строку и смотрим ее
echo "Исходная строка:\r\n";
$strRus = $argv[1];                                 // Возьмем исходную строку из параметра, пока без проверки на существование
echo "$strRus\r\n";
echo "\r\n";

// Преобразуем исходную строку и смотрим ее преобразование
echo "Преобразованная строка:\r\n";

$strTransUnderline = fTransUnder($strRus);          // Вызов функции преобразования строки
echo "$strTransUnderline\r\n";

?>