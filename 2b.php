<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Задание №2</title>
</head>
<body>
  <style>li{list-style-type: none;}</style>
  <h4>Задание №2</h4>
  <p>С помощью цикла do…while напишите функцию для вывода чисел от 0 до 10,
  чтобы результат выглядел так:</p>
  <ul>
    <li>0 – это ноль</li>
    <li>1 – нечетное число</li>
    <li>2 – четное число</li>
  </ul>
  <p>Решение:</p>
  <?php
    $i = 0;
    do {
        if ((($i % 2) == 0) AND ($i > 0)) {
            echo "$i - четное число<br>";
        }
        elseif (($i % 2) != 0) {
            echo "$i - нечетное число<br>";
        } else {
            echo "$i - это ноль<br>";
        }
        $i++;
    } while ($i <= 10)
  ?>
</body>
</html>