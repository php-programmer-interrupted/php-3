<?php
/* 
 * Напишите функцию, которая заменяет в строке пробелы на подчеркивания и
 * возвращает видоизмененную строчку.
 *
 * Пример вызова скрипта из командной строки: <php 7c.php "Преобразуемая строка">
 *
 */
/* Функция преобразования строки в массив, поддерживает UTF-8 */
function mb_str_split($string)
{
    return preg_split('/(?<!^)(?!$)/u', $string);
}
/*
 * Функция замены в строке пробелов на подчеркивания
 * В качестве параметра получает строку, возвращает преобразованную строку,
 * в которой пробелы заменены символом подчеркивания
 */
function ReplaceSpaceUnderline($strSpace = '')
{
    $arrStr = mb_str_split($strSpace);
    $strReturn = '';
    foreach ($arrStr as $strValue) {
        if ($strValue == ' ') {
            $strValue = '_';
        }
        $strReturn .= $strValue;
    }
    return $strReturn;
}

echo <<<TASK
Задание №7:
Напишите функцию, которая заменяет в строке пробелы на подчеркивания и
возвращает видоизмененную строчку.\r\n\r\n
TASK;
$strSpace = '';         // Исходная строка
$strUnderline = '';     // Измененная строка

// Получаем исходную строку и смотрим ее
echo "Исходная строка:\r\n";
$strSpace = $argv[1];              // Возьмем исходную строку из параметра
echo "$strSpace\r\n\r\n";
echo "Измененная строка:\r\n";
$strUnderline = ReplaceSpaceUnderline($strSpace);
echo $strUnderline;

?>

