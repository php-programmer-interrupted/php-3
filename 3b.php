<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Задание №3</title>
</head>
<body>
  <h4>Задание №3</h4>
  <p>Выведите с помощью цикла for числа от 0 до 9, НЕ
    используя тело цикла. То есть выглядеть должно вот так:
    for(…){// здесь пусто}</p>
  <p>Решение:</p>
  <?php
    for($i = 0; $i <= 9; print("i = $i<br>"), $i++);
  ?>
</body>
</html>