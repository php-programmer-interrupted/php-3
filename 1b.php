<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Задание №1</title>
</head>
<body>
  <h4>Задание №1</h4>
  <p>С помощью цикла while выведите все числа в промежутке от 0 до 100,
    которые делятся на 3 без остатка.</p>
  <p>Числа, которые делятся на 3 без остатка:</p>
  <?php
    $i = 0;
    while ($i <= 100) {
    if (($i % 3) == 0) {
        echo "$i ";
    }
    $i++;
    }
  ?>
</body>
</html>